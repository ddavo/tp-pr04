package simulator.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class PhysicsSimulator {
	private GravityLaws gl;
	private double dt;
	private double t;
	private List<Body> bodies;
	
	public PhysicsSimulator(GravityLaws gl, double tps) {
		this.gl = gl;
		this.dt = tps;
		this.t = 0;
		this.bodies = new ArrayList<>();
	}
	
	/** Aplica un paso de simulación, i.e., primero llama al método apply() de GravityLaws,
	 *  después llama a move(dt) para cada cuerpo, donde dt es el tiempo real por paso, y finalmente incrementa
	 *  el tiempo real en dt segundos */
	public void advance() {
		gl.apply(bodies);
		
		for (Body body : bodies) {
			body.move(dt);
		}
		
		t += dt;
	}
	
	/** Añade el cuerpo b al simulador. el método debe comprobar que no existe ningún otro cuerpo en el simulador
	 * con el mismo identificador. Si existiera, el método debe lanzar una excepción del tipo IllegalArgumentException
	 * @param b Cuerpo único
	 */
	public void addBody(Body b) {
		// Nota: Se ha sobreescrito equals
		if (bodies.contains(b)) throw new IllegalArgumentException();
		else bodies.add(b);
	}
	
	public String toString() {
		return dump().toString();
	}

	// Deberían estar PhysicsSimulator y Body implementados usando una interfaz
	// JSONDumpable o algo así?
	public JSONObject dump() {
		JSONObject dump = new JSONObject();
		dump.put("time", t);
		JSONArray jarray = new JSONArray();
		for (Body body : bodies) {
			jarray.put(body.getData());
		}
		dump.put("bodies", jarray);
		return dump;
	}
}
