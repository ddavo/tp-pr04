package simulator.factories;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Builder<T> {
	protected String _typeTag;
	protected String _desc;
	
	public Builder (String typeTag, String desc) {
		_typeTag = typeTag;
		_desc = desc;
	}
	
	/** Si la información suministrada por info es correcta, entonces crea un objeto de tipo T
	 * (i.e, una instancia de una subclase de T). En otro caso devuelve null para indicar que este
	 * constructor es incapaz de reconocer ese formato. En caso de que reconozca el campo type pero
	 * haya un error en alguno de los valores suministrados por la sección data, el método lanza una excepción
	 * IllegalArgumentException
	 * @param info
	 * @return
	 */
	public T createInstance(JSONObject info) {
		T b = null;
		if (_typeTag != null && _typeTag.equals(info.getString("type")))
			b = createTheInstance(info.getJSONObject("data"));
		return b;
	}
	
	/** Devuelve un objeto JSON que sirve de plantilla para el correspondiente constructor, i.e., un valor
	 * válido para el parámetro de createInstance {@link Factory#getInfo}
	 * @return
	 */
	public JSONObject getBuilderInfo() {
		JSONObject info = new JSONObject();
		info.put("type", _typeTag);
		info.put("data", createData());
		info.put("desc", _desc);
		return info;
	}
	
	// darray, jarray, farray
	protected double[] jsonArrayTodoubleArray(JSONArray jarray) {
		double[] darray = new double[jarray.length()];
		for (int i = 0; i < jarray.length(); ++i)
			darray[i] = jarray.getDouble(i);
		
		return darray;
	}
	
	protected JSONObject createData() {
		return new JSONObject();
	}
	
	protected abstract T createTheInstance(JSONObject jo);
}
