package simulator.control;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.json.*;

import simulator.factories.Factory;
import simulator.model.Body;
import simulator.model.PhysicsSimulator;

public class Controller {
	private PhysicsSimulator _psim;
	private Factory<Body> _bFac;
	
	public Controller(PhysicsSimulator pSim, Factory<Body> factory) {
		_psim = pSim;
		_bFac = factory;
	}
	
	/** Carga los bodies del json llamando al método addBody.
	 * Asumimos que in contiene una estructura JSON de la forma { "bodies" : [bb1, bb2...] }.
	 * Este método primero transforma la entrada JSON en un objeto JSONObject y luego extrae cada
	 * bb de JSONInput, crea el correspondiente cuerpo b usando la factoria de cuerpos y lo añade al simulador
	 * llamando al metodo addBody
	 *  */
	public void loadBodies(InputStream in) {
		JSONObject jsonInput = new JSONObject(new JSONTokener(in));
		JSONArray jarray = jsonInput.getJSONArray("bodies");
		for (int i = 0; i < jarray.length(); ++i) {
			_psim.addBody(_bFac.createInstance(jarray.getJSONObject(i)));
		}
	}
	
	public void run(int n) {
		run(n, null);
	}
	
	/** Ejecuta el simulador n pasos y muestra los diferentes estados en out usando en formato JSON
	 * <p>Nota: Para que la salida sea como la dada por el profesor, se debe imprimir el estado ANTES de
	 * ejecutar la simulación, y se debe avanzar hasta el último paso</p> */
	public void run(int n, OutputStream out) {
		PrintStream p = (out == null) ? new PrintStream(System.out) : new PrintStream(out);
		
		p.print("{ \"states\" : [ ");
		for (int i = 0; i <= n; ++i) {
			if (i != 0) p.print(", ");
			p.print(_psim.toString());
			_psim.advance();
		}
		p.print(" ] }");
		
		p.close();
	}
}
